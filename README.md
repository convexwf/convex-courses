# convex-courses

## 深蓝学院 Motion Planning for Mobile Robots 移动机器人运动规划

[teamo1996/Motion-plan: 深蓝运动规划课程](https://github.com/teamo1996/Motion-plan)

## CMU 15-445: Database Systems

[CMU 15-445/645 :: Intro to Database Systems (Fall 2019)](https://15445.courses.cs.cmu.edu/fall2019/schedule.html)
[CMU 15-445/645 :: Intro to Database Systems (Fall 2020)](https://15445.courses.cs.cmu.edu/fall2020/schedule.html)
[CMU 15-445/645 :: Intro to Database Systems (Fall 2021)](https://15445.courses.cs.cmu.edu/fall2021/schedule.html)
[paper_notes/db at master · ysj1173886760/paper_notes](https://github.com/ysj1173886760/paper_notes/tree/master/db)
[CMU 15-445 · ysj1173886760/Learning](https://github.com/ysj1173886760/Learning/tree/master/db)


### 课程简介

- 所属大学：CMU
- 先修要求：C++，数据结构与算法
- 编程语言：C++
- 预计学时：100 小时

作为 CMU 数据库的入门课，这门课由数据库领域的大牛 Andy Pavlo 讲授（“这个世界上我只在乎两件事，一是我的老婆，二就是数据库”）。15-445 会自底向上地教你数据库系统的基本组成部分：存储、索引、查询，以及并发事务控制。 这门课的亮点在于 CMU db 专门为此课开发了一个教学用的关系型数据库 bustub，并要求你对这个数据库的组成部分进行修改，实现上述部件的功能。此外 bustub 作为一个 C++ 编写的中小型项目涵盖了程序构建、代码规范、单元测试等众多要求，可以作为一个优秀的开源项目学习。

在 Fall2019 中，第二个 Project 是做哈希索引，第四个 Project 是做日志与恢复。

在 Fall2020 中，第二个 Project 是做 B 树，第四个 Project 是做并发控制。

在 Fall2021 中，第二个 Project 是做缓存池管理，第三个 Project 是做哈希索引，第四个 Project 是做并发控制。

另外在课程结束后，推荐阅读一篇论文 Architecture Of a Database System，对应的中文版也在上述仓库中。论文里综述了数据库系统的整体架构，让大家可以对数据库有一个更加全面的视野。

## MIT 6.S081: Operating System Engineering

[6.S081 课程网站/ Fall 2021](https://pdos.csail.mit.edu/6.828/2021/schedule.html)
[课程翻译文档 - MIT6.S081](https://mit-public-courses-cn-translatio.gitbook.io/mit6-s081/)
[课程教材 xv6: a simple, Unix-like teaching operating system](https://pdos.csail.mit.edu/6.828/2021/xv6/book-riscv-rev2.pdf)
[xv6 中文文档](https://th0ar.gitbooks.io/xv6-chinese/content/index.html)
[v6 操作系统的深入讲解](https://space.bilibili.com/1040264970/)
[Ko-oK-OS/xv6-rust: 🦀️ Re-implement xv6-riscv in Rust](https://github.com/Ko-oK-OS/xv6-rust)
[PKUFlyingPig/MIT6.S081-2020fall: MIT undergraduate operating system course](https://github.com/PKUFlyingPig/MIT6.S081-2020fall)
[KuangjuX/xv6-riscv-solution: MIT 6.S081 xv6-riscv solution](https://github.com/KuangjuX/xv6-riscv-solution)

### 课程简介

- 所属大学：麻省理工学院
- 先修要求：体系结构 + 扎实的 C 语言功底 + RISC-V 汇编语言
- 编程语言：C, RISC-V
- 预计学时：150 小时

麻省理工学院大名鼎鼎的 PDOS 实验室开设的面向MIT本科生的操作系统课程。开设这门课的教授之一 —— Robert Morris 教授曾是一位顶尖黑客，世界上第一个蠕虫病毒 Morris 就是出自他之手。

这门课的前身是 MIT 著名的课程 6.828，MIT 的几位教授为了这门课曾专门开发了一个基于 x86 的教学用操作系统 JOS，被众多名校作为自己的操统课程实验。但随着 RISC-V 的横空出世，这几位教授又基于 RISC-V 开发了一个新的教学用操作系统 xv6，并开设了 MIT6.S081 这门课。由于 RISC-V 轻便易学的特点，学生不需要像此前 JOS 一样纠结于众多 x86 “特有的”为了兼容而遗留下来的复杂机制，而可以专注于操作系统层面的开发。

这几位教授还专门写了一本教程，详细讲解了 xv6 的设计思想和实现细节。

这门课的讲授也很有意思，老师会带着学生依照 xv6 的源代码去理解操作系统的众多机制和设计细节，而不是停留于理论知识。每周都会有一个 lab，让你在 xv6 上增加一些新的机制和特性，非常注重学生动手能力的培养。整个学期一共有 11 个 lab，让你全方位地深刻理解操作系统的每个部分，非常有成就感。而且所有的lab都有着非常完善的测试框架，有的测试代码甚至上千行，让人不得不佩服 MIT 的几位教授为了教好这门课所付出的心血。

这门课的后半程会讲授操作系统领域的多篇经典论文，涉及文件系统、系统安全、网络、虚拟化等等多个主题，让你有机会接触到学界最前沿的研究方向。

## CMU 15-418/Stanford CS149: Parallel Computing

[Parallel Computer Architecture and Programming : 15-418/618 Spring 2016](http://15418.courses.cs.cmu.edu/spring2016/)
[PARALLEL COMPUTING Stanford CS149, Fall 2021](https://gfxcourses.stanford.edu/cs149/fall21)
[PKUFlyingPig/CS149-parallel-computing: Learning materials for Stanford CS149 : Parallel Computing](https://github.com/PKUFlyingPig/CS149-parallel-computing)

### 课程简介

- 所属大学：CMU 和 Stanford
- 先修要求：计算机体系结构，熟悉 C++
- 编程语言：C++
- 预计学时：150 小时

Kayvon Fatahalian 教授此前在 CMU 开了 15-418 这门课，后来他成为 Stanford 的助理教授后又开了类似的课程 CS149。但总体来说，15-418 包含的课程内容更丰富，并且有课程回放，但 CS149 的编程作业更 fashion 一些。
这门课会带你深入理解现代并行计算架构的设计原则与必要权衡，并学会如何充分利用硬件资源以及软件编程框架（例如 CUDA，MPI，OpenMP 等）编写高性能的并行程序。由于并行计算架构的复杂性，这门课会涉及诸多高级体系结构与网络通信的内容，知识点相当底层且硬核。与此同时，5 个编程作业则是从软件的层面培养学生对上层抽象的理解与运用，具体会让你分析并行程序的瓶颈、编写多线程同步代码、学习 CUDA 编程、OpenMP 编程以及前段时间大热的 Spark 框架等等。真正意义上将理论与实践完美地结合在了一起。

## MIT6.824: Distributed System

[6.824 Schedule: Spring 2022](https://pdos.csail.mit.edu/6.824/schedule.html)
[课程翻译文档 - MIT6.824](https://mit-public-courses-cn-translatio.gitbook.io/mit6-824/)

### 课程简介

- 所属大学：MIT
- 先修要求：计算机体系结构，并行编程
- 编程语言：Go
- 预计学时：200 小时

这门课和 MIT 6.S081 一样，出品自 MIT 大名鼎鼎的 PDOS 实验室，授课老师 Robert Morris 教授曾是一位顶尖黑客，世界上第一个蠕虫病毒 Morris 病毒就是出自他之手。

这门课每节课都会精读一篇分布式系统领域的经典论文，并由此传授分布式系统设计与实现的重要原则和关键技术。同时其课程 Project 也是以其难度之大而闻名遐迩，4 个编程作业循序渐进带你实现一个基于 Raft 共识算法的 KV-store 框架，让你在痛苦的 debug 中体会并行与分布式带来的随机性和复杂性。